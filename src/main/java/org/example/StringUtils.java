package org.example;

import static org.apache.commons.lang3.math.NumberUtils.toFloat;

public class StringUtils {

    public boolean isPositiveNumber(String str) {
        try {
            if (!str.isEmpty()) {
                return toFloat(str) > 0.0F;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
