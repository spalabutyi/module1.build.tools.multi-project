import org.example.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringUtilsTest {

    @Test
    @DisplayName(value = "Positive value Test")
    void isPositiveTest () {
        assertTrue(new StringUtils().isPositiveNumber("12"));
        assertTrue(new StringUtils().isPositiveNumber("12.33"));

        assertFalse(new StringUtils().isPositiveNumber("-12"));
        assertFalse(new StringUtils().isPositiveNumber("-12.33"));
        assertFalse(new StringUtils().isPositiveNumber("-12,33"));
        assertFalse(new StringUtils().isPositiveNumber("12,33"));

        assertFalse(new StringUtils().isPositiveNumber(""));
        assertFalse(new StringUtils().isPositiveNumber(" "));
        assertFalse(new StringUtils().isPositiveNumber("abcd"));
        assertFalse(new StringUtils().isPositiveNumber(null));
    }

}