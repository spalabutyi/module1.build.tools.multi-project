import org.example.StringUtils;

public class Utils {

    public boolean isAllPositiveNumbers(String... str) {

        for (String s : str) {
            if (!new StringUtils().isPositiveNumber(s)) {
                return false;
            }
        }
        return true;
    }
}
