import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static final Logger log = LogManager.getLogger(App.class);

    public static void main(String[] args) {

        log.info("Result is: {}", new Utils().isAllPositiveNumbers("12", "79"));
    }

}
